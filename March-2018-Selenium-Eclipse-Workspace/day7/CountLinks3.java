package day7;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CountLinks3 {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "/Users/mukeshotwani/Desktop/drivers/chromedriver36");

		WebDriver driver = new ChromeDriver();

		driver.get("http://demoqa.com/registration/");

		// Selenium always takes the first entry
		driver.findElement(By.name("radio_4[]")).click();

		List<WebElement> radiobuttons = driver.findElements(By.name("radio_4[]"));
		WebElement element = radiobuttons.get(0);
		element.click();

		driver.findElements(By.name("radio_4[]")).get(0).click();

		Thread.sleep(2000);

		driver.findElements(By.name("radio_4[]")).get(1).click();

		Thread.sleep(2000);

		driver.findElements(By.name("radio_4[]")).get(2).click();

		List<WebElement> allInput = driver.findElements(By.xpath("//input[@type='text']"));

		for (WebElement element1 : allInput) {
			element1.sendKeys("selenium");
		}

	}

}
