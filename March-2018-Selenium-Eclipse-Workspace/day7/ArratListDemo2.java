package day7;

import java.util.ArrayList;

public class ArratListDemo2 {

	public static void main(String[] args) {
		
		System.out.println("Start");
		
		ArrayList<String> names=new ArrayList<>();
		
		names.add("Selenium");
		
		names.add("WebDriver");
		
		names.add("Kabir");
		
		names.add("Aman");
		
		ArrayList<Integer> scores=new ArrayList<>();
		
		scores.add(12);
		
		scores.add(13);
		
		scores.add(14);
		
		scores.add(18);
		
	
		for(int a:scores)
		{
			System.out.println(a);
		
		}
		

		// Enhanced for loop 
		// For each loop
		// Advance for loop
		
		System.out.println("Basic for loop");
		for(int i=0;i<names.size();i++)
		{
			System.out.println("Names are "+names.get(i));
		}
		
		//Increment by 1
		System.out.println("Enhanced for loop");
		for(String mynames:names)
		{
			System.out.println(mynames);
		}
		
		
	}

}
