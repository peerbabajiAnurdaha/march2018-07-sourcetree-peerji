package day7;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CountLinks2 {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "/Users/mukeshotwani/Desktop/drivers/chromedriver");

		WebDriver driver = new ChromeDriver();

		driver.get("http://demoqa.com/registration/");
	
		int allInputbox=driver.findElements(By.tagName("input")).size();
		
		int allLinks=driver.findElements(By.tagName("a")).size();
		
		int allDropdown=driver.findElements(By.tagName("select")).size();
		
		int allTextarea=driver.findElements(By.tagName("textarea")).size();
		
		System.out.println("Total input "+allInputbox +" "+ "Links "+allLinks +" "+"Total Dropdown "+allDropdown);
		
		
	}

}
