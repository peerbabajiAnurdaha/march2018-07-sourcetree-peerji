package day7;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CountLinks {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "/Users/mukeshotwani/Desktop/drivers/chromedriver");

		WebDriver driver = new ChromeDriver();

		driver.get("http://demoqa.com/");

		List<WebElement> links=driver.findElements(By.xpath("//a[@href]"));
		
		int count=links.size();
		
		if(count==29)
		{
			System.out.println("Validation pass");
		}
		
		for(WebElement myLink:links)
		{
			System.out.println("Href of link is "+myLink.getAttribute("href"));
			System.out.println("Title of link is "+myLink.getAttribute("title"));
			System.out.println("Visible Text of link is "+myLink.getAttribute("innerHTML"));
			
			System.out.println("****-----------------****");
		}
		
		
		List<WebElement> allDropdowns=	driver.findElements(By.xpath("//select"));
		
		for(WebElement mydropdown:allDropdowns)
		{
			System.out.println("id of drodown is "+mydropdown.getAttribute("id"));	
		}
	    
		
		
	}

}
