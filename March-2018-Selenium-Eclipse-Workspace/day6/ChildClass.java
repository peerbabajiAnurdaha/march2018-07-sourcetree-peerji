package day6;

public class ChildClass extends ParentClass {

	public static void main(String[] args) 
	{
		// Scenario 1
		ChildClass obj1=new ChildClass();
				
		obj1.carloanemi();
		
		obj1.sum();
		
		obj1.sub();
		
		obj1.mul();
		
		obj1.div();
		
		// Scenario 2
		ParentClass obj2=new ParentClass();
	    obj2.div();
	    
	    // Scenario 3 - even if you created object of child class but reference is still holding to parent class
	    // then you can only call the baseclass method
	    ParentClass obj3=new ChildClass();  
	    obj3.div(); 
	    Object obj4=new ChildClass();  
	    
	    // Scenario 4- not applicable
	   // ChildClass obj5=(ChildClass) new ParentClass();
	  
	}
	
	
	public void carloanemi()
	{
		System.out.println("Ur car EMI is 30000 INR");
	}

}
