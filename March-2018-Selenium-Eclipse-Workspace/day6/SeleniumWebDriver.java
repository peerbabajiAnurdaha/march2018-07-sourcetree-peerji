package day6;

public interface SeleniumWebDriver 
{
	
	// Constant which cant be changed in interface
	String aut_name="Steve";
	
	
	public void get(String url);
	
	public void quit();
	
	public String getCurrentURL();

}
