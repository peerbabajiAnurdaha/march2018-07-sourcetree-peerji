package day2;

public class doWhileLoop2 {

	public static void main(String[] args) {

		int i = 10;

		do 
		{
			System.out.println("value is " + i);
			i++;
		} while (i <= 20);

	}

}
