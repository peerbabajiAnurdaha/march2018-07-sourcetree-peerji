/**
 * 
 */
package day2;

/**
 * @author mukeshotwani
 * 
 *         This class will guide you how to create object and how to call
 *         methods using Class
 *
 */
public class OOPDemo {

	public static void main(String[] args) 
	{
		OOPDemo abc = new OOPDemo();

		abc.calcPersonalEMI();

		abc.calcCarEMI();

		abc.calcHomeEMI();

	}

	public void calcHomeEMI() {
		System.out.println("Hey HOME Loan EMI is 50000 INR");
	}

	public void calcCarEMI() {
		System.out.println("Hey CAR Loan EMI is 20000 INR");
	}

	public void calcPersonalEMI() {
		System.out.println("Hey Personal Loan EMI is 10000 INR");
	}

}
