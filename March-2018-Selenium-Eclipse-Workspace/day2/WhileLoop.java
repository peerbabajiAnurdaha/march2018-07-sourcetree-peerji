package day2;

public class WhileLoop {

	public static void main(String[] args) {
		
		int i=10;
		
		while (i<=30) {
			
			System.out.println("Value is "+i);
			i++;
		}
		
		
	}

}
