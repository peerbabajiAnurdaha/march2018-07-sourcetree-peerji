package day2;

public class SwitchStatement {

	public static void main(String[] args) {

		System.out.println("Program Started");

		int a = 66660;

		switch (a) {
		case 10:
			System.out.println("Value is 10");
			break;
		case 20:
			System.out.println("Value is 20");
			break;
		case 30:
			System.out.println("Value is 30");
			break;
		case 40:
			System.out.println("Value is 40");
			break;

		default: System.out.println("Please provide correct number");
			break;
		}

	}

}
