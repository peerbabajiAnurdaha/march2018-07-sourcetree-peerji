package ass2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class demoqa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "/Users/sarika/eclipse-workspace/chromedriver-2.36/chromedriver");

		ChromeDriver driver = new ChromeDriver();

		driver.get("http://demoqa.com/");

		if (driver.getTitle().contains("WordPress site")) {
			System.out.println("Validation 1 Passed : Title Verified");
		} else {
			System.out.println("Validation 1 Failed : Title not verified");
		}

		WebElement linkd = driver.findElementByXPath("//a[text()='Registration']");

		boolean reglinkEnabled = linkd.isEnabled();

		if (reglinkEnabled) {
			System.out.println("Validation 2 Passed: Registration Lnk Enabled");
		} else {
			System.out.println("Validation 2 Failed: Registration Lnk not Enabled");
		}

		String regLinkUrl = linkd.getAttribute("href");

		if (regLinkUrl.contains("registration")) {
			System.out.println("Validation 3: Registration URL is working fine");
			linkd.click();
		} else {
			System.out.println("Validation 3: Registration URL is not working");
		}
		driver.findElementByName("first_name").sendKeys("sarika");

		driver.findElementByName("last_name").sendKeys("bagga");

		driver.findElementByXPath("//input[@value=\"single\"]").click();

		driver.findElementByXPath("//input[@value=\"reading\"]").click();

		WebElement country = driver.findElementById("dropdown_7");

		Select countryNames = new Select(country);
		
		countryNames.selectByValue("India");
		
		WebElement date = driver.findElementById("mm_date_8");
		Select dates = new Select(date);	
		dates.selectByValue("12");
		
		WebElement day = driver.findElementById("dd_date_8");
		Select days = new Select(day);	
		days.selectByValue("25");
		
		WebElement year = driver.findElementById("yy_date_8");
		Select years = new Select(year);	
		years.selectByValue("1987");
		
		driver.findElementById("phone_9").sendKeys("917355074193");

		driver.findElementById("username").sendKeys("sarikabagga786");

		driver.findElementByName("e_mail").sendKeys("sarika7@gmail.com");

		driver.findElementByName("profile_pic_10")
				.sendKeys("/Users/sarika/Downloads/New Folder With Items/sarika-pic.png");

		driver.findElementByName("description").sendKeys("selenium test");

		driver.findElementByName("password").sendKeys("sarika7777");

		driver.findElementById("confirm_password_password_2").sendKeys("sarika7777");

		driver.findElementByName("pie_submit").click();

	}

}
