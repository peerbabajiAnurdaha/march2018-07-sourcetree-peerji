package ass1;

import org.openqa.selenium.chrome.ChromeDriver;

public class Prg6 {

	public static void main(String[] args) throws InterruptedException {
	
		System.out.println("Starts Chrome Browser");
		
		System.setProperty("webdriver.chrome.driver","/Users/sarika/eclipse-workspace/chromedriver-2.31/chromedriver");
		
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("https://www.google.co.in");
		Thread.sleep(2000);
		
		driver.navigate().refresh();
		
		Thread.sleep(2000);
		
		driver.executeScript("history.go(0)");
		Thread.sleep(2000);
		
		driver.navigate().to(driver.getCurrentUrl());
	}

}
