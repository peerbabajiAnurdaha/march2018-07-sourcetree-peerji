package day3;

import org.openqa.selenium.chrome.ChromeDriver;

public class VerifyPage {

	public static void main(String[] args) {

		//System.setProperty("webdriver.chrome.driver", "/Users/mukeshotwani/Desktop/drivers/chromedriver");
		System.setProperty("webdriver.chrome.driver","/Users/sarika/eclipse-workspace/chromedriver-2.31/chromedriver");
		
		ChromeDriver driver = new ChromeDriver();

		driver.get("http://learn-automation.com/");

		String url = driver.getCurrentUrl();

		boolean siteurl = url.contains("automation");

		if (siteurl) {
			System.out.println("Site url is correct and Validation 1 passed");
		} else {
			System.out.println("Site url is incorrect and Validation 1 failed");
		}

		String title = driver.getTitle();

		String expectedTitle = "Selenium WebDriver tutorial - Selenium WebDriver tutorial Step by Step";

		boolean titlestatus = title.equalsIgnoreCase(expectedTitle);

		if (titlestatus) {
			System.out.println("Site Title is correct and Validation 2 passed");
		} else {
			System.out.println("Site Title is incorrect and Validation 2 failed");
		}

		String src = driver.getPageSource();

		if (src.contains("www.adasd.com")) {
			System.out.println("Site has YouTube Channel and Validation 3 passed");
		} else {
			System.out.println("Site does not have YouTube Channel and Validation 3 failed");
		}

		driver.quit();
		
	}

}
