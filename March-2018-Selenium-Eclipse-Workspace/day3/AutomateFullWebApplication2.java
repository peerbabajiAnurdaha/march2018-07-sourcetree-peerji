package day3;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutomateFullWebApplication2 {

	public static void main(String[] args) {

		//System.setProperty("webdriver.chrome.driver", "/Users/mukeshotwani/Desktop/drivers/chromedriver");
		System.setProperty("webdriver.chrome.driver","/Users/sarika/eclipse-workspace/chromedriver-2.31/chromedriver");
		
		ChromeDriver driver = new ChromeDriver();

		driver.get("http://demoqa.com/registration/");
		
		WebElement radio = driver.findElementByXPath("//*[@id='pie_register']/li[2]/div/div/input[1]");
		
		System.out.println("Is element enabled "+radio.isEnabled());
		
		System.out.println("State of Radio  button "+radio.isSelected());
	
	}

}
