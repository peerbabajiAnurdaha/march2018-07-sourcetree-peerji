package day3;

import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserCommands {

	public static void main(String[] args) 
	{
	
		//System.setProperty("webdriver.chrome.driver","/Users/mukeshotwani/Desktop/drivers/chromedriver");
		System.setProperty("webdriver.chrome.driver","/Users/sarika/eclipse-workspace/chromedriver-2.31/chromedriver");
		
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("http://learn-automation.com/");
		
		String url=driver.getCurrentUrl();
		
		System.out.println("Current url is "+url);
		
		String title=driver.getTitle();
		
		System.out.println("Current Title is "+title);
		
		String src=driver.getPageSource();
		
		System.out.println("Current Title is "+src);
		

	}

}
