package day3;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutomateFullWebApplication {

	public static void main(String[] args) {

		//System.setProperty("webdriver.chrome.driver", "/Users/mukeshotwani/Desktop/drivers/chromedriver");
		System.setProperty("webdriver.chrome.driver","/Users/sarika/eclipse-workspace/chromedriver-2.31/chromedriver");
		
		ChromeDriver driver = new ChromeDriver();

		driver.get("http://demoqa.com/");

		// WebElement link=driver.findElementByLinkText("Registration");

		// link.click();

		WebElement link = driver.findElementByPartialLinkText("Registra");

		link.click();

		WebElement name = driver.findElementById("name_3_firstname");

		name.sendKeys("Selenium");

		WebElement radio = driver.findElementByXPath("//*[@id='pie_register']/li[2]/div/div/input[1]");

		radio.click();

		WebElement pic = driver.findElementById("profile_pic_10");

		pic.sendKeys("/Users/mukeshotwani/Desktop/Class_3/Class 2.pptx");

		WebElement button = driver.findElementByName("pie_submit");

		button.click();

	}

}
