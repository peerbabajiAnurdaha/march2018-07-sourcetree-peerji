package day3;

import org.openqa.selenium.chrome.ChromeDriver;

public class NavigationCommands {

	public static void main(String[] args) 
	{
	
		//System.setProperty("webdriver.chrome.driver","/Users/mukeshotwani/Desktop/drivers/chromedriver");
		System.setProperty("webdriver.chrome.driver","/Users/sarika/eclipse-workspace/chromedriver-2.31/chromedriver");
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("http://learn-automation.com/");
		
		driver.get("https://www.google.co.in/");
		
		driver.navigate().back();
		
		driver.navigate().forward();
		
		driver.navigate().refresh();
		
		
	}

}
