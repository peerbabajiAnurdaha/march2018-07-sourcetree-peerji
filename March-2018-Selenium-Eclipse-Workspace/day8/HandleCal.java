package day8;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandleCal {

	public static void main(String[] args) throws InterruptedException 
	{
	
		System.setProperty("webdriver.chrome.driver", "/Users/mukeshotwani/Desktop/drivers/chromedriver36");

		WebDriver driver = new ChromeDriver();

		driver.get("http://seleniumpractise.blogspot.in/2016/08/how-to-handle-calendar-in-selenium.html");

		driver.findElement(By.id("datepicker")).click();
		
		Thread.sleep(2000);
		
		List<WebElement> allDates=driver.findElements(By.xpath("(//table[@class='ui-datepicker-calendar'])[1]//a"));

		System.out.println("Total Dates for current month "+allDates.size());
		
		for(WebElement date:allDates)
		{
			String mydate=date.getAttribute("innerHTML");
			
			System.out.println("Dates are "+mydate);
			
			if(mydate.equalsIgnoreCase("19"))
			{
				date.click();
				System.out.println("Date Selected");
				break;
			}
		}
		
	}

}
