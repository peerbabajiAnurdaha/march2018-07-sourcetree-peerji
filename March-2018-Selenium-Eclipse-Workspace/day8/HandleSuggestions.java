package day8;

// Type Exact value
// Type keyword and if suggestion comes then mouse hover and click
// Type keyword check all suggestion, traverse and click on desired value


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandleSuggestions {

	public static void main(String[] args) throws InterruptedException 
	{
	
		System.setProperty("webdriver.chrome.driver", "/Users/mukeshotwani/Desktop/drivers/chromedriver36");

		WebDriver driver = new ChromeDriver();

		driver.get("http://seleniumpractise.blogspot.in/2016/08/how-to-handle-autocomplete-feature-in.html");
		
		driver.findElement(By.id("tags")).sendKeys("S");
		
		Thread.sleep(2000);
		
		List<WebElement> allValues=driver.findElements(By.xpath("//ul[contains(@class,'ui-menu ui-widget ui-widget-content ui-autocomplete')]//li//div"));
		
		System.out.println("Total records coming as suggestion is "+allValues.size());
		
		for(WebElement ele:allValues)
		{
			
			String values=ele.getAttribute("innerHTML");
			
			System.out.println("Suggestions values are "+values);
			
			if(values.equalsIgnoreCase("Selenium"))
			{
				ele.click();
				System.out.println("Record found and Clicked");
				break;
			}
		}
		
	}

}
