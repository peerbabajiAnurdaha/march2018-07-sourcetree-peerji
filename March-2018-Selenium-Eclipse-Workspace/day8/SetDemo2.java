package day8;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo2 {

	public static void main(String[] args) {
		
		Set<String> names=new HashSet<>();
		
		names.add("Selenium");
		names.add("WebDriver");
		names.add("AnnaAnto");
		names.add("Selenium");
		names.add("Jenkins");
		names.add("Maven");
		names.add("TestNG");
		
		Iterator<String> oldnames=names.iterator();
		
		while(oldnames.hasNext())
		{
			String value=oldnames.next();
			
			System.out.println("New Techno "+value);
		}
		

	}

}
