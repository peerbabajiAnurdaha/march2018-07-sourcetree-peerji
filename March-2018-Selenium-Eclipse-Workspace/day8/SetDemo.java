package day8;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class SetDemo {

	public static void main(String[] args) {
		
		HashSet<String> names=new HashSet<>();
		
		names.add("Selenium");
		names.add("WebDriver");
		names.add("AnnaAnto");
		names.add("Selenium");
		names.add("Jenkins");
		names.add("Maven");
		names.add("TestNG");
		
		for(String s:names)
		{
			System.out.println(s);
		}
		
		LinkedHashSet<String> nameSelenium=new LinkedHashSet<>();
		
		nameSelenium.add("Aman");
		nameSelenium.add("Anto");
		nameSelenium.add("Niya");
		nameSelenium.add("Kabir");
		nameSelenium.add("Parth");
		nameSelenium.add("Paul");
		nameSelenium.add("Ram");
		
		System.out.println(nameSelenium);
		
		
		TreeSet<String> nameSelenium1=new TreeSet<>();
		
		nameSelenium1.add("Aman");
		nameSelenium1.add("Anto");
		nameSelenium1.add("Niya");
		nameSelenium1.add("Kabir");
		nameSelenium1.add("Parth");
		nameSelenium1.add("Zeba");
		nameSelenium1.add("Yuvraj");
		
		System.out.println(nameSelenium1);
	}

}
