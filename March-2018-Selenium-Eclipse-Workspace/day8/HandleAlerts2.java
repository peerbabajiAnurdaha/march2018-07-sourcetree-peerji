package day8;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandleAlerts2 {

	public static void main(String[] args) throws InterruptedException 
	{
	
		System.setProperty("webdriver.chrome.driver", "/Users/mukeshotwani/Desktop/drivers/chromedriver36");

		WebDriver driver = new ChromeDriver();

		driver.get("http://www.ksrtc.in/oprs-web/");
		
		driver.findElement(By.name("searchBtn")).click();
		
		Thread.sleep(2000);
		
		//Alert alt=driver.switchTo().alert();
		
		// Accept, Dismiss, GetText, Enter values
		
		String altText=driver.switchTo().alert().getText();
		
		System.out.println("Alert text is "+altText);
		
		// This will handle or accept the alert
		driver.switchTo().alert().accept();
		
		// This will dismiss the alert or close the alert
		//alt.dismiss();
		
		// This will pass the values in Alert box
		//alt.sendKeys("");
	
	}

}
