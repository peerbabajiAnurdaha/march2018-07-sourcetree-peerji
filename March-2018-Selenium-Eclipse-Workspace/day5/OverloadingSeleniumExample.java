package day5;

// Constructor should have same name as ClassName
// Constructor do not have any return type not even void
// It will invoke automatically once you create object


public class OverloadingSeleniumExample {

	public static void main(String[] args) 
	{
		System.out.println("Start");
		OverloadingSeleniumExample obj=new OverloadingSeleniumExample();
		obj.emi();
		System.out.println("End");
	}
	
	public OverloadingSeleniumExample()
	{
		System.out.println("Hey Mukesh Welcome to Learn Automation");
	}
	
	public OverloadingSeleniumExample(String Name)
	{
		System.out.println("Hey "+Name+" Welcome to Learn Automation");
	}
	
	public OverloadingSeleniumExample(String first,String second)
	{
		System.out.println("Hey "+first +" "+second +" Welcome to Learn Automation");
	}
	
	
	
	public void emi()
	{
		System.out.println("Your EMI is 50000 INR");
	}
	
}
