package day5;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDownExample {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","/Users/sarika/eclipse-workspace/chromedriver-2.36/chromedriver");
		ChromeDriver driver = new ChromeDriver();

		driver.get("http://demoqa.com/registration/");
	
		WebElement country=driver.findElementById("dropdown_7");
		
		Select countryNames=new Select(country);
		
		boolean status=countryNames.isMultiple();
		
		System.out.println("Dropdown is multiSelected "+status);
		
		WebElement name=countryNames.getFirstSelectedOption();
		
		// getText()= This method will return you the name of option which is visible in Dropdown
		
		String c_name=name.getText();
		
		System.out.println(c_name);

		countryNames.selectByIndex(9);
		
		Thread.sleep(3000);
		
		countryNames.selectByValue("Ireland");
		
		Thread.sleep(3000);
		
		countryNames.selectByVisibleText("Mauritius");
		
		WebElement name1=countryNames.getFirstSelectedOption();
		
		// getText()= This method will return you the name of option which is visible in Dropdown
		
		String c_name1=name1.getText();
		
		System.out.println(c_name1);
	
		driver.quit();
	}

}
