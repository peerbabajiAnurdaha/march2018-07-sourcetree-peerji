package day5;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDownExample2 {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver","/Users/sarika/eclipse-workspace/chromedriver-2.36/chromedriver");

		ChromeDriver driver = new ChromeDriver();

		driver.get("http://demoqa.com/registration/");
		
		WebElement country=driver.findElementByXPath("//*[text()='Country']//following::select[1]");
			
		country.sendKeys("Ireland");
		
	}

}
