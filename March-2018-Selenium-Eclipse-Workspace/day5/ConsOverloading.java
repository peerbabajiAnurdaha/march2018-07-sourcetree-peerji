package day5;

import org.openqa.selenium.chrome.ChromeDriver;

// Constructor should have same name as ClassName
// Constructor do not have any return type not even void
// It will invoke automatically once you create object


public class ConsOverloading {

	public static void main(String[] args) 
	{
		System.out.println("Start");
				
		ConsOverloading obj=new ConsOverloading();
		
		obj.emi();
		
		ConsOverloading obj1=new ConsOverloading("Mukesh");
		
		obj1.emi();
		
		ConsOverloading obj2=new ConsOverloading("Paul","Selenium");
		
		obj2.emi();
		
		System.out.println("End");
	}
	
	public ConsOverloading()
	{
		System.out.println("Hey Mukesh Welcome to Learn Automation");
	}
	
	public ConsOverloading(String Name)
	{
		System.out.println("Hey "+Name+" Welcome to Learn Automation");
	}
	
	public ConsOverloading(String first,String second)
	{
		System.out.println("Hey "+first +" "+second +" Welcome to Learn Automation");
	}
	
	
	
	public void emi()
	{
		System.out.println("Your EMI is 50000 INR");
	}
	
}
