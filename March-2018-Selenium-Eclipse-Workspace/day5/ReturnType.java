package day5;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ReturnType {

	public static void main(String[] args)  {
		
		
		
		
		
		ReturnType obj=new ReturnType();
	
		double sum=obj.add(12.35, 12);
		
		System.out.println("Sum of double values is "+sum);
		
		int suma=obj.add(34, 56);
		
		System.out.println("Sum of int values "+suma);
		
		String[] aaa=obj.names();
		
		System.out.println(aaa[0] +" "+aaa[1]);
	}
	
	
	public void add()
	{
		System.out.println("Add value with no return");
	}
	
	public int add(int a,int b)
	{
		int c=a+b;	
		
		return c;
	}
	
	public int add(int a,int b,int c)
	{
		return a+b+c;
	}
	
	public double add(double a,int b)
	{
		double c=a+b;	
		
		return c;
	}
	
	public  String[] names()
	{
		String []names=new String[2];
		names[0]="Selenium";
		names[1]="WebDriver";	
		return names;
	}

}
