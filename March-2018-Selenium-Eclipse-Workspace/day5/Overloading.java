package day5;

// Size 
// Type Limitaion

public class Overloading {

	public static void main(String[] args) {
		
		Overloading obj1=new Overloading();
		obj1.add("Vydehi","Sangeeta");
		obj1.add(12, 13);
	}
	
	public void add()
	{
		
	}
	
	public void add(int a)
	{
		
	}
	
	public void add(int a,int b)
	{
		int c=a+b;
		System.out.println("Sum is "+ c);
	}
	
	public void add(String a,String b)
	{
		System.out.println("Names are "+a + " "+b);
	}
	
	public void add(int a,double b)
	{
		
	}
	
	public void add(double a,int b)
	{
		
	}

}
